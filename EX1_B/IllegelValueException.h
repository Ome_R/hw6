#pragma once

#include <iostream>

class IllegelValueException : public std::exception {
public:
	virtual char const* what() const;
};
