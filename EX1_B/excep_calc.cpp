#include <iostream>
#include "IllegelValueException.h"

#define ILLEGEL_VALUE 8200

int add(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE || a + b == ILLEGEL_VALUE)
		throw IllegelValueException();

	return a + b;
}

int multiply(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		throw IllegelValueException();

	int sum = 0;

	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == ILLEGEL_VALUE)
			throw IllegelValueException();
	}

	return sum;
}

int pow(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		throw IllegelValueException();

	int exponent = 1;

	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == ILLEGEL_VALUE)
			throw IllegelValueException();
	}

	return exponent;
}

void printAddResult(int a, int b) {
	try {
		std::cout << add(a, b) << std::endl;
	}
	catch (const IllegelValueException& ex) {
		std::cout << "This user is not authorized to access 8200, " <<
			"please enter different numbers, or try to get clearance in 1 year." << std::endl;
	}
}

void printMultiplyResult(int a, int b) {
	try {
		std::cout << multiply(a, b) << std::endl;
	}
	catch (const IllegelValueException& ex) {
		std::cout << "This user is not authorized to access 8200, " <<
			"please enter different numbers, or try to get clearance in 1 year." << std::endl;
	}
}

void printPowerResult(int a, int b) {
	try {
		std::cout << pow(a, b) << std::endl;
	}
	catch (const IllegelValueException& ex) {
		std::cout << "This user is not authorized to access 8200, " <<
			"please enter different numbers, or try to get clearance in 1 year." << std::endl;
	}
}

int main(void) {
	//Checking add method
	std::cout << "Add check:" << std::endl;
	printAddResult(ILLEGEL_VALUE, 5); //Should print error
	printAddResult(5, ILLEGEL_VALUE); //Should print error
	printAddResult(ILLEGEL_VALUE - 5, 5); //Should print error
	printAddResult(ILLEGEL_VALUE + 1, 5); //Shouldn't

	std::cout << std::endl << "Multiply check:" << std::endl;

	//Checking multiply method
	printMultiplyResult(ILLEGEL_VALUE, 5); //Should print error
	printMultiplyResult(5, ILLEGEL_VALUE); //Should print error
	printMultiplyResult(ILLEGEL_VALUE / 5, 5); //Should print error
	printMultiplyResult(ILLEGEL_VALUE + 1, 5); //Shouldn't

	std::cout << std::endl << "Power check:" << std::endl;

	//Checking pow method
	printPowerResult(ILLEGEL_VALUE, 5); //Should print error
	printPowerResult(5, ILLEGEL_VALUE); //Should print error
	printPowerResult(ILLEGEL_VALUE + 1, 5); //Should print error - at some point one of the values will be 8200
	printPowerResult(5, 5); //Shouldn't

	system("pause");

	return 0;
}