#include <iostream>
#include "Ille"

#define ILLEGEL_VALUE 8200

int	add(int a, int b) {
	if(a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		throw Illegel
	return a + b;
}

int multiply(int a, int b) {
	int sum = 0;

	for(int i = 0; i < b; i++) {
		sum = add(sum, a);
	}

	return sum;
}

int pow(int a, int b) {
	int exponent = 1;

	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
	}

	return exponent;
}

int main(void) {
	std::cout << pow(5, 5) << std::endl;
}