#include <iostream>

#define ILLEGEL_VALUE 8200

int add(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		return ILLEGEL_VALUE;
	return a + b;
}

int multiply(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		return ILLEGEL_VALUE;

	int sum = 0;

	for(int i = 0; i < b; i++) {
		sum = add(sum, a);
		if (sum == ILLEGEL_VALUE)
			return ILLEGEL_VALUE;
	}

	return sum;
}

int pow(int a, int b) {
	if (a == ILLEGEL_VALUE || b == ILLEGEL_VALUE)
		return ILLEGEL_VALUE;

	int exponent = 1;

	for(int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		if (exponent == ILLEGEL_VALUE)
			return ILLEGEL_VALUE;
	}

	return exponent;
}

void printResult(int result) {
	if (result == ILLEGEL_VALUE) {
		std::cout << "This user is not authorized to access 8200, " <<
			"please enter different numbers, or try to get clearance in 1 year." << std::endl;
	}
	else {
		std::cout << result << std::endl;
	}
}

int main(void) {
	//Checking add method
	std::cout << "Add check:" << std::endl;
	printResult(add(ILLEGEL_VALUE, 5)); //Should print error
	printResult(add(5, ILLEGEL_VALUE)); //Should print error
	printResult(add(ILLEGEL_VALUE - 5, 5)); //Should print error
	printResult(add(ILLEGEL_VALUE + 1, 5)); //Shouldn't

	std::cout << std::endl << "Multiply check:" << std::endl;

	//Checking multiply method
	printResult(multiply(ILLEGEL_VALUE, 5)); //Should print error
	printResult(multiply(5, ILLEGEL_VALUE)); //Should print error
	printResult(multiply(ILLEGEL_VALUE / 5, 5)); //Should print error
	printResult(multiply(ILLEGEL_VALUE + 1, 5)); //Shouldn't

	std::cout << std::endl << "Power check:" << std::endl;

	//Checking pow method
	printResult(pow(ILLEGEL_VALUE, 5)); //Should print error
	printResult(pow(5, ILLEGEL_VALUE)); //Should print error
	printResult(pow(ILLEGEL_VALUE + 1, 5)); //Should print error - at some point one of the values will be 8200
	printResult(pow(5, 5)); //Shouldn't

	system("pause");

	return 0;
}