#include "Rectangle.h"
#include "ShapeException.h"
#include <iostream>

Rectangle::Rectangle(std::string name, std::string color, int width, int height) : Quadrilateral(name, color, width, height) {
}

void Rectangle::draw()
{
	std::cout << "Name is " << getName() << std::endl 
		<< "Color is " << getColor() << std::endl 
		<< "Height is " << getHeight() << std::endl 
		<< "Width is " << getWidth() << std::endl
		<< "Area is "<< getArea() << std::endl 
		<< "Is square (1,0)?: " << isSquare() << std::endl;
}

bool Rectangle::isSquare() {
	return getHeight() == getWidth();
}