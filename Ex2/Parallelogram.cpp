#include "Parallelogram.h"
#include "Shape.h"
#include "ShapeException.h"
#include <iostream>


Parallelogram::Parallelogram(std::string color, std::string name, int height, int width, double firstAngle, double secondAngle) : Quadrilateral(color, name, height, width) {
	 setAngles(firstAngle, secondAngle);
}

double Parallelogram::getFirstAngle() {
	return _firstAngle;
}

double Parallelogram::getSecondAngle() {
	return _secondAngle;
}

void Parallelogram::setAngles(double firstAngle, double secondAngle) {
	if (firstAngle < 0 || firstAngle > 180 || secondAngle < 0 || secondAngle > 180)
		throw ShapeException();
	_firstAngle = firstAngle;
	_secondAngle = secondAngle;
}

void Parallelogram::draw(){
	std::cout << "Name is " << getName() << std::endl 
		<< "Color is " << getColor() << std::endl 
		<< "Height is " << getHeight() << std::endl 
		<< "Width is " << getWidth() << std::endl 
		<< "Angles are " << getFirstAngle() << ", " << getSecondAngle() << std::endl 
		<< "Area is "<< getArea() << std::endl
		<< "Perimeter is " << getPerimeter() << std::endl;
}
