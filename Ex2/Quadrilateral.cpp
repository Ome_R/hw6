#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

Quadrilateral::Quadrilateral(std::string name, std::string color, int height, int width) : Shape(name, color) {
	setHeight(height);
	setWidth(width);
}

int Quadrilateral::getHeight() {
	return _height;
}

int Quadrilateral::getWidth() {
	return _width;
}

double Quadrilateral::getArea(){
	return _width * _height;
}

double Quadrilateral::getPerimeter() {
	return 2 * (_height + _width);
}

void Quadrilateral::setHeight(int height) {
	if (height < 0)
		throw ShapeException();
	_height = height;
}

void Quadrilateral::setWidth(int width) {
	if (width < 0)
		throw ShapeException();
	_width = width;
}

void Quadrilateral::draw() {
	std::cout << "Name is " << getName() << std::endl 
		<< "Color is " << getColor() << std::endl 
		<< "Width is " << getWidth() << std::endl 
		<< "Height is " << getHeight() << std::endl 
		<< "Area is "<< getArea() << std::endl 
		<< "Perimeter is "<< getPerimeter() << std::endl;
}