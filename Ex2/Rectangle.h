#pragma once

#include "Quadrilateral.h"
#include <iostream>

class Rectangle : public Quadrilateral {
public:
	// C O N S T R U C T O R S
	Rectangle(std::string name, std::string color, int height, int width);

	// O T H E R  M E T H O D S
	virtual void draw();
	bool isSquare();
};