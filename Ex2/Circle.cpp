#include "Shape.h"
#include "Circle.h"
#include "ShapeException.h"

#include <iostream>

#define PI 3.14

Circle::Circle(std::string name, std::string color, double radius): Shape(name, color) {
	setRadius(radius);		
}

double Circle::getRadius() {
	return _radius;
}

double Circle::getPerimeter() {
	return 2 * PI * _radius;
}

double Circle::getArea() {
	return PI * _radius * _radius;
}

void Circle::setRadius(double radius) {
	if (radius < 0)
		throw ShapeException();
	_radius = radius;
}

void Circle::draw() {
	std::cout << "Name is "<< getName() << std::endl
		<< "Color is " << getColor() << std::endl
		<< "Radius is "<< getRadius() << std::endl
		<< "Perimeter is "<< getPerimeter() << std::endl;
}