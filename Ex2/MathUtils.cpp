#include "MathUtils.h"

#define PENTAGON_FACTOR 1.72048
#define HEXAGON_FACTOR 2.59808

double MathUtils::calcPentagonArea(double side)
{
	return PENTAGON_FACTOR * side * side;
}

double MathUtils::calcHexagonArea(double side)
{
	return HEXAGON_FACTOR * side * side;
}
