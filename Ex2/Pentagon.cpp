#include "Pentagon.h"
#include "ShapeException.h"

Pentagon::Pentagon(std::string name, std::string color, double side) : Shape(name, color)
{
	setSide(side);
}

double Pentagon::getSide()
{
	return _side;
}

double Pentagon::getArea()
{
	return MathUtils::calcPentagonArea(_side);
}

double Pentagon::getPerimeter()
{
	//Not used
	return 0.0;
}

void Pentagon::setSide(double side)
{
	if (side < 0)
		throw ShapeException();
	_side = side;
}

void Pentagon::draw()
{
	std::cout << "Name is " << getName() << std::endl
		<< "Color is " << getColor() << std::endl
		<< "Side is " << getSide() << std::endl
		<< "Area is " << getArea() << std::endl;
}
