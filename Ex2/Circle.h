#pragma once

#include "Shape.h"
#include <iostream>

class Circle : public Shape {
public:
	// C O N S T R U C T O R S
	Circle(std::string name, std::string color, double radius);

	// G E T T E R S
	double getRadius();
	virtual double getArea();
	virtual double getPerimeter();

	// S E T T E R S
	void setRadius(double radius);

	// O T H E R  M E T H O D S
	virtual void draw();
private:
	double _radius;
};