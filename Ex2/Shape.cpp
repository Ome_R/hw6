#include "Shape.h"

#include <iostream>

Shape::Shape(std::string name, std::string color){
	setName(name);
	setColor(color);
	_count++;
}

std::string Shape::getName() {
	return _name;
}

Shape::~Shape(){
	_count--;
}

std::string Shape::getColor() {
	return _color;
}

void Shape::setName(std::string name) {
	_name = name;
}

void Shape::setColor(std::string color) {
	_color = color;
}

int Shape::_count = 0;

int Shape::getCount()
{
	return _count;
}
