#pragma once

#include "Shape.h"
#include "MathUtils.h"

class Pentagon : public Shape {
public:
	// C O N S T R U C T O R S
	Pentagon(std::string name, std::string color, double side);

	// G E T T E R S
	double getSide();
	virtual double getArea();
	virtual double getPerimeter();

	// S E T T E R S
	void setSide(double side);

	// O T H E R  M E T H O D S
	virtual void draw();
private:
	double _side;
};