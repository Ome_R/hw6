#pragma once

#include "Shape.h"
#include <iostream>

class Quadrilateral : public Shape {
public:
	// C O N S T R U C T O R S
	Quadrilateral(std::string name, std::string color, int height, int width);
	
	// G E T T E R S
	int getHeight();
	int getWidth();
	virtual double getArea();
	virtual double getPerimeter();
	
	// S E T T E R S
	void setHeight(int height);
	void setWidth(int width);

	// O T H E R  M E T H O D S
	virtual void draw();
private:
	int _width, _height;
};