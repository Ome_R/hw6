#pragma once

class MathUtils {
public:
	static double calcPentagonArea(double side);
	static double calcHexagonArea(double side);
};