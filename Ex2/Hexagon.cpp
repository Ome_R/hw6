#include "Hexagon.h"
#include "ShapeException.h"

Hexagon::Hexagon(std::string name, std::string color, double side) : Shape(name, color)
{
	setSide(side);
}

double Hexagon::getSide()
{
	return _side;
}

double Hexagon::getArea()
{
	return MathUtils::calcHexagonArea(_side);
}

double Hexagon::getPerimeter()
{
	//Not used
	return 0.0;
}

void Hexagon::setSide(double side)
{
	if (side < 0)
		throw ShapeException();
	_side = side;
}

void Hexagon::draw()
{
	std::cout << "Name is " << getName() << std::endl
		<< "Color is " << getColor() << std::endl
		<< "Side is " << getSide() << std::endl
		<< "Area is " << getArea() << std::endl;
}
