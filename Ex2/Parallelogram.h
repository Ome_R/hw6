#pragma once

#include "Quadrilateral.h"
#include <iostream>

class Parallelogram : public Quadrilateral {
public:
	// C O N S T R U C T O R S
	Parallelogram(std::string color, std::string name, int height, int width, double firstAngle, double secondAngle);
	
	// G E T T E R S
	double getFirstAngle();
	double getSecondAngle();

	// S E T T E R S
	void setAngles(double firstAngle, double secondAngle);

	// O T H E R  M E T H O D S
	virtual void draw();
private:
	double _firstAngle, _secondAngle;
};