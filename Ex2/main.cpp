#include "Shape.h"
#include "Circle.h"
#include "Quadrilateral.h"
#include "Rectangle.h"
#include "Parallelogram.h"
#include "Pentagon.h"
#include "Hexagon.h"
#include "InputException.h"

#include <iostream>
#include <string>

using std::string;
using std::exception;
using std::cout;
using std::cin;
using std::endl;

int main() {
	// S H A P E S  D A T A
	string name, color, shapeType;
	double radius = 0, firstAngle = 0, secondAngle = 180, side = 0;
	int height = 0, width = 0;

	// S H A P E  O B J E C T S
	Circle circle(name, color, radius);
	Quadrilateral quadrilateral(name, color, width, height);
	Rectangle rectangle(name, color, width, height);
	Parallelogram parallelogram(name, color, width, height, firstAngle, secondAngle);
	Pentagon pentagon(name, color, side);
	Hexagon hexagon(name, color, side);

	cout << "Enter information for your objects:" << endl;

	while (shapeType != "x") {
		cout << "Which shape would you like to work with?..." << endl 
			<< "c = circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, pe = pentagon" << endl;
		cin >> shapeType;
		try{
			if (shapeType == "c") {
				cout << "Enter name, color and radius for circle:" << endl;
				if (!(cin >> name >> color >> radius))
					throw InputException();
				circle.setColor(color);
				circle.setName(name);
				circle.setRadius(radius);
				circle.draw();
			}
			else if (shapeType == "q") {
				cout << "Enter name, color, height and width for quadrilateral:" << endl;
				if (!(cin >> name >> color >> height >> width))
					throw InputException();
				quadrilateral.setName(name);
				quadrilateral.setColor(color);
				quadrilateral.setHeight(height);
				quadrilateral.setWidth(width);
				quadrilateral.draw();
			}
			else if (shapeType == "r") {
				cout << "Enter name, color, height and width for rectangle:" << endl;
				if (!(cin >> name >> color >> height >> width))
					throw InputException();
				rectangle.setName(name);
				rectangle.setColor(color);
				rectangle.setHeight(height);
				rectangle.setWidth(width);
				rectangle.draw();
			}
			else if (shapeType == "p") {
				cout << "Enter name, color, height, width and 2 angles for parallelogram:" << endl;
				if (!(cin >> name >> color >> height >> width >> firstAngle >> secondAngle))
					throw InputException();
				parallelogram.setName(name);
				parallelogram.setColor(color);
				parallelogram.setHeight(height);
				parallelogram.setWidth(width);
				parallelogram.setAngles(firstAngle, secondAngle);
				parallelogram.draw();
			}
			else if (shapeType == "pe") {
				cout << "Enter name, color and side for pentagon:" << endl;
				if (!(cin >> name >> color >> side))
					throw InputException();
				pentagon.setName(name);
				pentagon.setColor(color);
				pentagon.setSide(side);
				pentagon.draw();
			}
			else if (shapeType == "h") {
				cout << "Enter name, color and side for hexagon:" << endl;
				if (!(cin >> name >> color >> side))
					throw InputException();
				hexagon.setName(name);
				hexagon.setColor(color);
				hexagon.setSide(side);
				hexagon.draw();
			}
			else if (shapeType.length() > 1) {
				cout << "Warning - Don't try to build more than one shape at once" << endl;
			}
			else {
				cout << "You have entered an invalid letter, please re-enter" << endl;
			}

			cout << "Would you like to add more object? Press any key, if not press x" << endl;
			cin >> shapeType;
		}
		catch (InputException& ex) {
			cin.clear(); //clear bad input flag
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); //discard input
			cout << ex.what() << endl;
		}
		catch (exception& ex) {
			cout << ex.what() << endl;
		}
		catch (...){
			cout << "Cought a bad exception. Cotinuing as usual..." << endl;
		}
	}

	system("pause");
	return 0;
}