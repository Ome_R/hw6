#pragma once

#include <iostream>
#include <string>

class Shape {
public:
	// C O N S T R U C T O R S
	Shape(std::string name, std::string color);
	~Shape();

	// G E T T E R S
	std::string getName();
	std::string getColor();
	virtual double getArea() = 0;
	virtual double getPerimeter() = 0;

	// S E T T E R S
	void setName(std::string name);
	void setColor(std::string color);

	// O T H E R  M E T H O D S
	virtual void draw() = 0;

	// S T A T I C  M E T H O D S
	static int getCount();
private:
	static int _count;
	std::string _name;
	std::string _color;
};